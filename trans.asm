assume cs:code, ds:data
data segment
	mat	dw	1, 2, 3, 4, 5, 6
	rows	dw	2
	cols	dw	3
	n	dw	6
data ends
code segment
print macro num
	mov ax, num
	add ax, "0"
	mov dx, ax
	mov ah, 2h
	int 21h
endm
start:
	mov ax,data
	mov ds,ax

	mov bx, 0
loopi:	cmp bx, n
	jge extloopi
		mov cx, rows
		mov si, bx
		looprow: mov ax, mat[si]
			 print ax
				 ;print space
				 mov dx, 0h
				 mov ah, 02h
				 int 21h
			 add si, cols
			 add si, cols ;need to add 2 times since elements take up 1 word
			 loop looprow
		;print newline
		mov dx, 10
		mov ah, 2h
		int 21h
		inc bx
		inc bx ;also need to increment 2 times
		jmp loopi
extloopi:
	mov ah, 4ch
	int 21h
code ends
end start

assume cs:code, ds:data
data segment
	arr	db	51,99,10,45,53,31
	n	dw	6
data ends
code segment
;takes number in ax
;modifies ax, bx, cx, dx
proc print_num
	mov cx, 2	;2 digit
pshlop:	mov dx, 0
	mov bx, 10
	div bx
	push dx
	loop pshlop
	mov cx, 2
poplop:	pop dx
	add dx, "0"
	mov ah, 2h
	int 21h
	loop poplop
	ret
endp
start:	mov ax, data
	mov ds, ax

	mov si, n
loopi:	dec si
	cmp si, 0
	jle extlop
	;ax is max
	mov al, arr[si]
	mov cx, si
	mov bx, cx
	; di is position of current max-to-be
	mov di, bx
	; decrease number of comparisons in each loop
	loopj:	
		dec bx
		cmp al, arr[bx]
		jg  noxchg
			xchg al, arr[bx]
			mov arr[di], al
	noxchg:	loop loopj
	jmp loopi
extlop:	;print sorted arr
	mov cx, n
loopk:	mov bx, n
	sub bx, cx
	mov al, arr[bx]
	mov ah, 0
	push cx
	call print_num
	pop cx
	;print space
	mov dx, 0h
	mov ah, 02h
	int 21h
	loop loopk

	mov ah, 4ch
	int 21h
code ends
end start

assume cs:code, ds:data
data segment
	asc	dw	67
data ends
code segment
; changes dx and ax
print macro num
	mov ax, num
	mov dl, 10
	div dl
	add ax, "00"
	mov dx, ax
	mov ah, 2h
	mov dl, dh
	int 21h
endm
start:	mov ax, data
	mov ds, ax

	; compare asc with ascii for 9, if greater go somewhere 
	mov ax, asc
	cmp ax, 57
	jg  atof
	; else
	;asc between 48 and 57
	sub ax, 48
	print ax
	mov ah, 4ch
	int 21h

atof:	;assuming ascii between 65 and 71
	mov bx, ax
	sub ax, 65
	add ax, 0ah
	mov dx, bx
	mov ah, 2h
	int 21h
	
	mov ah, 4ch
	int 21h
code ends
end start

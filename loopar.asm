assume cs:code, ds:data
data segment
	arr	dw	1, 2, 3, 4, 5, 6
	n	dw	6
data ends
code segment
print macro num
	mov ax, num
	add ax, "0"
	mov dx, ax
	mov ah, 2h
	int 21h
	mov dx, 0h
	mov ah, 2h
	int 21h
endm
start:	mov ax, data
	mov ds, ax

	mov cx, n
	mov si, 0
loopi:	mov ax, arr[si]
	print ax
	inc si
	inc si
	loop loopi

	mov ah, 4ch
	int 21h
code ends
end start
